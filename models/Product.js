const mongoose = require('mongoose')
const { Schema } = mongoose
const productsScema = Schema({
  name: String,
  price: Number
})

module.exports = mongoose.model('Product', productsScema)
